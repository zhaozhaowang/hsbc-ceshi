package com.hsbc.bean;

import lombok.Data;

@Data
public class Book {
    private Integer bookid;
    private Integer card;
}
