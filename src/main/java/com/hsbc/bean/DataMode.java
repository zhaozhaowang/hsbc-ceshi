package com.hsbc.bean;

import lombok.Data;

@Data
public class DataMode<T> {
    private String name;
    private int age;
}
