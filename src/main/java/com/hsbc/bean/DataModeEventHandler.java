package com.hsbc.bean;


import com.lmax.disruptor.EventHandler;

public class DataModeEventHandler implements EventHandler<DataModeEvent> {

    @Override
    public void onEvent(DataModeEvent event, long sequence, boolean endOfBatch) throws Exception {
        //1、收到数据
        //2、mybatis执行
        System.out.println(event.getValue());
    }
}
