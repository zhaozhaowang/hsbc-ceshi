package com.hsbc.bean;

import com.lmax.disruptor.EventFactory;

public class DataModeEventFactory implements EventFactory<DataModeEvent> {

    @Override
    public DataModeEvent newInstance() {
        return new DataModeEvent();
    }
}
