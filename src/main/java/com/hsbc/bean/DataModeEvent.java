package com.hsbc.bean;

import lombok.Data;

@Data
public class DataModeEvent {

    private DataMode value;
}

