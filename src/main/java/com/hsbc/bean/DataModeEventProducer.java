package com.hsbc.bean;


import com.lmax.disruptor.RingBuffer;

public class DataModeEventProducer {

    private final RingBuffer<DataModeEvent> ringBuffer;

    public DataModeEventProducer(RingBuffer<DataModeEvent> ringBuffer) {
        this.ringBuffer = ringBuffer;
    }

    public void onData(DataMode data) {
        long next = ringBuffer.next();
        try {
            DataModeEvent dataModeEvent = ringBuffer.get(next);
            dataModeEvent.setValue(data);
        } finally {
            ringBuffer.publish(next);
        }
    }
}
