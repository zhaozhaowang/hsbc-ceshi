package com.hsbc.config;

import com.hsbc.bean.*;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.util.DaemonThreadFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class DisruptorConfig {

    @Bean("DataModeEventProducer")
    public DataModeEventProducer producer() {
        DataModeEventFactory dataModeEventFactory = new DataModeEventFactory();
        int bufferSize = 1024 * 1024;
        Disruptor<DataModeEvent> disruptor = new Disruptor<>(dataModeEventFactory, bufferSize, DaemonThreadFactory.INSTANCE);
        disruptor.handleEventsWith(new DataModeEventHandler());
        disruptor.start();
        RingBuffer<DataModeEvent> ringBuffer = disruptor.getRingBuffer();
        DataModeEventProducer producer = new DataModeEventProducer(ringBuffer);
        return producer;
    }
}


