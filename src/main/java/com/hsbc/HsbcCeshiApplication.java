package com.hsbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HsbcCeshiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HsbcCeshiApplication.class, args);
	}

}
