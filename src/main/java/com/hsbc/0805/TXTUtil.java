package com.hsbc.utils;

import com.alibaba.excel.EasyExcel;
import com.hsbc.bean.excel.OfferExcel;
import com.hsbc.entity.Request2ResponseParam;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class TXTUtil {

    private static String DIRECTORY_PATH = "G:\\IdeaProjects\\MX-0805\\src\\main\\resources\\";

    private static String EXCEL_FILE_PATH = "C:\\Users\\Administrator\\Desktop\\" + System.currentTimeMillis() + ".xlsx";

    private static List<Request2ResponseParam> lists = new ArrayList<>();

    /**
     * 提取URL里的参数
     *
     * @param strUrl
     * @return
     */
    private static void resolverApiUrl(Request2ResponseParam obj, String strUrl) {
        strUrl = strUrl.trim().toLowerCase();
        String[] arrSplit = strUrl.split("[?]");
        String strAllParam = arrSplit[1];
        arrSplit = strAllParam.split("[&]");

        for (String strSplit : arrSplit) {
            String[] arrSplitEqual = null;
            arrSplitEqual = strSplit.split("[=]");
            //解析出键值
            if (arrSplitEqual.length > 1) {
                //正确解析
                // 遍历输出属性
                Field[] fields = obj.getClass().getDeclaredFields();
                for (int i = 0; i < fields.length; i++) {
                    Field f = fields[i];
                    f.setAccessible(true);

                    String s = arrSplitEqual[0].replace("\"", "").replace("\"", "");
                    String parameter = f.getName();

                    if (s.equalsIgnoreCase(parameter)) {
                        try {
                            f.set(obj, arrSplitEqual[1].replace("\"", "").replace("\"", ""));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }
            } else {
                if (arrSplitEqual[0] != "") {
                    //只有参数没有值，不加入
                }
            }
        }
    }

    /**
     * 解析TXT文本内容
     *
     * @param filePath
     */
    private static void readTxtFile(String filePath) {
        try {
            //构建对象 为了创建Excel
            Request2ResponseParam obj = new Request2ResponseParam();

            File file = new File(filePath);
            //判断文件是否存在
            if (file.isFile() && file.exists()) {
                InputStreamReader read = new InputStreamReader(
                        new FileInputStream(file), "UTF-8");
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = null;
                while ((lineTxt = bufferedReader.readLine()) != null) {
                    if (lineTxt.startsWith("http") || lineTxt.startsWith("https")) {
                        resolverApiUrl(obj, lineTxt);
                    } else {
                        String[] arrResponseLine = null;
                        arrResponseLine = lineTxt.split("[:]");
                        if (arrResponseLine.length > 1) {
                            // 遍历输出属性
                            Field[] fields = obj.getClass().getDeclaredFields();
                            for (int i = 0; i < fields.length; i++) {
                                Field f = fields[i];
                                f.setAccessible(true);
                                String s = arrResponseLine[0].replace("\"", "").replace("\"", "");
                                String parameter = f.getName();
                                if (s.equalsIgnoreCase(parameter)) {
                                    f.set(obj, arrResponseLine[1].replace("\"", "").replace("\"", ""));
                                    break;
                                }
                            }
                        } else {
                            if (arrResponseLine[0] != "") {
                                //只有参数没有值，不加入
                                //request2ResponseParam
                            }
                        }
                    }
                }
                lists.add(obj);
                read.close();
            } else {
                System.out.println("找不到指定的文件");
            }
        } catch (Exception e) {
            System.out.println("读取文件内容出错");
            e.printStackTrace();
        }
    }

    /**
     * 获取指定目录下的所有TXT文件 不递归
     *
     * @param directoryPath
     * @return
     */
    private static List<String> getFiles(String directoryPath) {
        List<String> files = new ArrayList<>();
        File file = new File(directoryPath);
        File[] tempList = file.listFiles();
        for (int i = 0; i < tempList.length; i++) {
            if (tempList[i].isFile()) {
                files.add(tempList[i].toString());
            }
        }
        return files;
    }


    public static void main(String argv[]) {
        List<String> filelist = getFiles(DIRECTORY_PATH);
        for (int i = 0; i < filelist.size(); i++) {
            readTxtFile(filelist.get(i));
        }
        System.out.println(lists);
        EasyExcel.write(EXCEL_FILE_PATH, Request2ResponseParam.class).sheet("MX").doWrite(lists);
    }
}