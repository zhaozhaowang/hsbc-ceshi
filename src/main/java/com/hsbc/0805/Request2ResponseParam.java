package com.hsbc.entity;


import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Request2ResponseParam {
    @ExcelProperty(index = 0, value = "indicator")
    public String indicator = "";

    @ExcelProperty("agency")
    public String agency = "";

    @ExcelProperty("batchNumber")
    public String batchNumber = "";

    @ExcelProperty("offerSequence")
    public String offerSequence = "";

    @ExcelProperty("refNumber")
    public String refNumber = "";

    @ExcelProperty("parentRefNumber")
    public String parentRefNumber = "";

    @ExcelProperty("code")
    public String code = "";

    @ExcelProperty("numberOfMerchants")
    public String numberOfMerchants = "";
}
