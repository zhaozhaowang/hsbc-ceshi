package com.hsbc.controller;

import com.hsbc.bean.DataMode;
import com.hsbc.bean.DataModeEventProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

@RestController
public class HelloController {

    @Autowired
    private DataModeEventProducer producer;

    @GetMapping("/say")
    public void sayHello(){

        //造假发送数据
        DataMode<Object> objectDataMode = new DataMode<>();
        objectDataMode.setAge(18);
        ConcurrentHashMap<Object, Object> concurrentHashMap = new ConcurrentHashMap<>();
        concurrentHashMap.put("abc", 123);
        ArrayList<Object> data = new ArrayList<>();
        data.add(concurrentHashMap);
        //造假发送数据

        //发送数据
        producer.onData(objectDataMode);
    }
}
