package com.hsbc.mapper;

import com.hsbc.bean.Book;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface BookMapper {
    Book getBookById(Integer bookid);
}
