package com.hsbc;

import com.hsbc.bean.Book;
import com.hsbc.mapper.BookMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HsbcCeshiApplicationTests {

	@Autowired
	private BookMapper bookMapper;

	@Test
	void contextLoads() {
		Book book = bookMapper.getBookById(1);
		System.out.println(book);
	}

}
